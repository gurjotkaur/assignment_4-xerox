/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ajay,Gurjot,Harsh
 */
public class AnalysisHelper {
 public void topProducts() {
        LinkedHashMap<Integer, Integer> topproducts = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Item> item = DataStore.getInstance().getItem();

        for (Item i : item.values()) {

            if (topproducts.containsKey(i.getProdId())) {

                int quantity = topproducts.get(i.getProdId());
                topproducts.put(i.getProdId(), (quantity + i.getQuantity()));

            } else {
                topproducts.put(i.getProdId(), i.getQuantity());
            }
        }

        List<Integer> mapKeys = new ArrayList<>(topproducts.keySet());
        List<Integer> mapValues = new ArrayList<>(topproducts.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = topproducts.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> prod = new ArrayList<Integer>(sortedMap.keySet());
        Collections.reverse(prod);
        System.out.println("\nTop 3 Products\r");
        for (int x = 0; x < 3; x++) {

            System.out.println("Product " + prod.get(x));
        }

    }

    public void topCustomers() {
        LinkedHashMap<Integer, Integer> topcustomers = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();

        for (Order o : orders.values()) {
            if (topcustomers.containsKey(o.getCustomerId())) {
                int sum = topcustomers.get(o.getCustomerId());
                topcustomers.put(o.getCustomerId(), (sum) + (o.getItem().getQuantity() * o.getItem().getSalesPrice()));

            } else {
                topcustomers.put(o.getCustomerId(), o.getItem().getQuantity() * o.getItem().getSalesPrice());
            }

        }

        List<Integer> mapKeys = new ArrayList<>(topcustomers.keySet());
        List<Integer> mapValues = new ArrayList<>(topcustomers.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = topcustomers.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> customer = new ArrayList<Integer>(sortedMap.keySet());
        Collections.reverse(customer);
        System.out.println("\nTop 3 Customers\r");
        for (int x = 0; x < 3; x++) {

            System.out.println("Customer " + customer.get(x));
        }

    }

    public void topSalesPerson() {
        
        LinkedHashMap<Integer, Integer> topSalesPerson = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
      
        for (Order o : orders.values()) {
            int prod = o.getItem().getProdId();

            for (Product p : products.values()) {
                if (p.getProductId() == prod) {
                    {

                        if (topSalesPerson.containsKey(o.getSalesId())) {
                            int sum = topSalesPerson.get(o.getSalesId());
                            topSalesPerson.put(o.getSalesId(), (sum) + (o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice())));

                        } else {
                            topSalesPerson.put(o.getSalesId(), o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice()));

                        }
                    }
                }
            }
        }

        List<Integer> mapKeys = new ArrayList<>(topSalesPerson.keySet());
        List<Integer> mapValues = new ArrayList<>(topSalesPerson.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = topSalesPerson.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> salesPerson = new ArrayList<Integer>(sortedMap.keySet());
        Collections.reverse(salesPerson);
        System.out.println("\nTop 3 SalesPerson\r");
        for (int x = 0; x < 3; x++) {

            System.out.println("SalesPerson " + salesPerson.get(x));
        }

    }

    public void totalRevenue() {
      
        LinkedHashMap<Integer, Integer> totalRevenue = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();

        for (Order o : orders.values()) {
            int prod = o.getItem().getProdId();

            for (Product p : products.values()) {
                if (p.getProductId() == prod) {
                    {

                        if (totalRevenue.containsKey(o.getOrderId())) {
                            int sum = totalRevenue.get(o.getOrderId());
                            totalRevenue.put(o.getOrderId(), (sum) + (o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice())));

                        } else {
                            totalRevenue.put(o.getOrderId(), o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice()));

                        }
                    }
                }
            }
        }
        int sum = 0;
        for (int i = 0; i < totalRevenue.size(); i++) {
            sum = sum + totalRevenue.get(i);
        }

        System.out.println("\nTotal Revenue generated with respect to Sales Data: " + sum);
    }
  

}
